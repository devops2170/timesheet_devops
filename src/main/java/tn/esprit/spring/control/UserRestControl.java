package tn.esprit.spring.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.entities.User;
import tn.esprit.spring.services.IUserService;


@RestController

public class UserRestControl {


    @Autowired
    IUserService userService;

    @GetMapping("/retrieve-all-users")
    public List<User> retrieveAllUsers() {
        List<User> list = userService.retrieveAllUsers();
        return list;
    }

    @GetMapping("/retrieve-user/{user-id}")
    public User retrieveUser(@PathVariable("user-id") String userId) {
        return userService.retrieveUser(userId);
    }

    @PostMapping("/add-user")
    public User addUser(@RequestBody User u) {
        User user = userService.addUser(u);
        return user;
    }

    @DeleteMapping("/remove-user/{user-id}")
    public void removeUser(@PathVariable("user-id") String userId) {
        userService.deleteUser(userId);
    }

    // Modifier User
    @PutMapping("/modify-user")
    public User updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

} 
 