package tn.esprit.spring.control;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.services.IEmployeService;

@RestController
public class RestControlEmploye {

    @Autowired
    IEmployeService iemployeservice;


    @PostMapping("/ajouterEmployer")
    public Employe ajouterEmploye(@RequestBody Employe employe) {
        iemployeservice.addOrUpdateEmploye(employe);
        return employe;
    }

    @PutMapping(value = "/modifyEmail/{id}/{newemail}")
    public void mettreAjourEmailByEmployeId(@PathVariable("newemail") String email, @PathVariable("id") Long employeId) {
        iemployeservice.mettreAjourEmailByEmployeId(email, employeId);
    }

    @PutMapping(value = "/affecterEmployeADepartement/{idemp}/{iddept}")
    public void affecterEmployeADepartement(@PathVariable("idemp") Long employeId, @PathVariable("iddept") int depId) {
        iemployeservice.affecterEmployeADepartement(employeId, depId);

    }

    @PutMapping(value = "/affecterContratAEmploye/{idcontrat}/{idemp}")
    public void affecterContratAEmploye(@PathVariable("idcontrat") int contratId, @PathVariable("idemp") Long employeId) {
        iemployeservice.affecterContratAEmploye(contratId, employeId);
    }


    @GetMapping(value = "getEmployePrenomById/{idemp}")
    public String getEmployePrenomById(@PathVariable("idemp") Long employeId) {
        return iemployeservice.getEmployePrenomById(employeId);
    }

    @DeleteMapping("/deleteEmployeById/{idemp}")
    public void deleteEmployeById(@PathVariable("idemp") Long employeId) {
        iemployeservice.deleteEmployeById(employeId);

    }


    @GetMapping(value = "getNombreEmployeJPQL")
    public int getNombreEmployeJPQL() {

        return iemployeservice.getNombreEmployeJPQL();
    }

    @GetMapping(value = "getAllEmployeNamesJPQL")
    public List<String> getAllEmployeNamesJPQL() {

        return iemployeservice.getAllEmployeNamesJPQL();
    }


    @PutMapping(value = "/mettreAjourEmailByEmployeIdJPQL/{id}/{newemail}")
    @ResponseBody
    public void mettreAjourEmailByEmployeIdJPQL(@PathVariable("newemail") String email, @PathVariable("id") Long employeId) {
        iemployeservice.mettreAjourEmailByEmployeId(email, employeId);
    }

    @GetMapping(value = "getSalaireByEmployeIdJPQL/{idemp}")
    @ResponseBody
    public float getSalaireByEmployeIdJPQL(@PathVariable("idemp") Long employeId) {
        return iemployeservice.getSalaireByEmployeIdJPQL(employeId);
    }

    @GetMapping(value = "getSalaireMoyenByDepartementId/{iddept}")
    @ResponseBody
    public Double getSalaireMoyenByDepartementId(@PathVariable("iddept") int departementId) {
        return iemployeservice.getSalaireMoyenByDepartementId(departementId);
    }


    @GetMapping(value = "/getAllEmployes")
    @ResponseBody
    public List<Employe> getAllEmployes() {

        return iemployeservice.getAllEmployes();
    }


}

