package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;

import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;


@Service
public class EmployeServiceImpl implements IEmployeService {

    private static final Logger l = LogManager.getLogger(EmployeServiceImpl.class);

    @Autowired
    EmployeRepository employeRepository;
    @Autowired
    DepartementRepository deptRepoistory;
    @Autowired
    ContratRepository contratRepoistory;


    public void mettreAjourEmailByEmployeId(String email, Long employeId) {
        try {

            Employe employe = employeRepository.findById(employeId).orElse(null);
            if (employe != null) {
                employe.setEmail(email);
                employeRepository.save(employe);
            }
        } catch (Exception e) {
            l.error("Erreur lors de la mise à jour du mail");
        }

    }

    @Transactional
    public void affecterEmployeADepartement(Long employeId, int depId) {
        try {
            Departement depManagedEntity = deptRepoistory.findById(depId).orElse(null);
            Employe employeManagedEntity = employeRepository.findById(employeId).orElse(null);
            if (depManagedEntity != null) {
                if (depManagedEntity.getEmployes() == null) {

                    List<Employe> employes = new ArrayList<>();
                    employes.add(employeManagedEntity);
                    depManagedEntity.setEmployes(employes);
                } else {

                    depManagedEntity.getEmployes().add(employeManagedEntity);
                }

                deptRepoistory.save(depManagedEntity);
            }
        } catch (Exception e) {
            l.error("Erreur lors de l'affectation employé departement");
        }

    }

    @Transactional
    public void desaffecterEmployeDuDepartement(int employeId, int depId) {
        try {
            Departement dep = deptRepoistory.findById(depId).orElse(null);
            if (dep != null) {
                int employeNb = dep.getEmployes().size();
                for (int index = 0; index < employeNb; index++) {
                    if (dep.getEmployes().get(index).getId() == employeId) {
                        dep.getEmployes().remove(index);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            l.error("Erreur lors de la désaffectation employé departement");
        }
    }

    @Override
    public Long ajouterContrat(Contrat contrat) {
        try {
            contratRepoistory.save(contrat);
        } catch (Exception exp) {
            l.error("Ajout contrat error", exp.getMessage());
        }
        l.info("Contrat Ajouté avec succée");
        return contrat.getReference();
    }


    public void affecterContratAEmploye(int contratId, Long employeId) {
        try {
            Contrat contratManagedEntity = contratRepoistory.findById(contratId).orElse(null);
            Employe employeManagedEntity = employeRepository.findById(employeId).orElse(null);
            if (contratManagedEntity != null && employeManagedEntity != null) {
                contratManagedEntity.setEmploye(employeManagedEntity);
                contratRepoistory.save(contratManagedEntity);
            }
        } catch (Exception e) {
            l.error("Erreur lors de l'affectation employé Contrat");
        }

    }

    public String getEmployePrenomById(Long employeId) {
        Employe employeManagedEntity = employeRepository.findById(employeId).orElse(null);
        if (employeManagedEntity != null) {
            return employeManagedEntity.getPrenom();
        } else
            return null;
    }

    public void deleteEmployeById(Long employeId) {
        employeRepository.deleteById(employeId);
    }

    public void deleteContratById(int contratId) {
        Contrat contratManagedEntity = contratRepoistory.findById(contratId).orElse(null);
        if (contratManagedEntity != null) {
            contratRepoistory.delete(contratManagedEntity);
        } else {
            l.error("Contrat may be NULL");
        }
    }


    public int getNombreEmployeJPQL() {
        return employeRepository.countemp();
    }

    public List<String> getAllEmployeNamesJPQL() {
        return employeRepository.employeNames();

    }

    public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
        return employeRepository.getAllEmployeByEntreprisec(entreprise);
    }

    public void deleteAllContratJPQL() {
        employeRepository.deleteAllContratJPQL();
    }

    public float getSalaireByEmployeIdJPQL(Long employeId) {
        return employeRepository.getSalaireByEmployeIdJPQL(employeId);
    }

    public Double getSalaireMoyenByDepartementId(int departementId) {
        return employeRepository.getSalaireMoyenByDepartementId(departementId);
    }

    public List<Employe> getAllEmployes() {
        return (List<Employe>) employeRepository.findAll();
    }

    public Long addOrUpdateEmploye(Employe employe) {
        employeRepository.save(employe);
        return employe.getId();
    }

    public Employe getEmployeById(Long id) {
        return employeRepository.findById(id).orElse(null);
    }

}