package tn.esprit.spring.services;

import java.util.List;
import java.util.Optional;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;


public interface IEmployeService {
    void mettreAjourEmailByEmployeId(String email, Long employeId);

    void affecterEmployeADepartement(Long employeId, int depId);

    Long ajouterContrat(Contrat contrat);

    void affecterContratAEmploye(int contratId, Long employeId);

    String getEmployePrenomById(Long employeId);

    void deleteEmployeById(Long employeId);

    void deleteContratById(int contratId);

    int getNombreEmployeJPQL();

    List<String> getAllEmployeNamesJPQL();

    List<Employe> getAllEmployeByEntreprise(Entreprise entreprise);

    float getSalaireByEmployeIdJPQL(Long employeId);

    Double getSalaireMoyenByDepartementId(int departementId);

    List<Employe> getAllEmployes();

    Long addOrUpdateEmploye(Employe employe);

    Employe getEmployeById(Long id);


}
