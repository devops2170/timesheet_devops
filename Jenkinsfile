pipeline {
    environment{
      registry = "oamri/timesheet"
      registryCredential = 'oamri'
      dockerImage = ''
    }
    agent any
    stages {
        stage("Compile") {
            steps {
              updateGitlabCommitStatus name: 'Compile', state: 'running'
              bat "mvn clean compile -DskipTests -X"
            }
            post {
                success {
                  updateGitlabCommitStatus name: 'Compile', state: 'success'
                }
                failure {
                  updateGitlabCommitStatus name: 'Compile', state: 'failed'
                }
            }
        }
        
        stage("Tests") {
          steps {
            updateGitlabCommitStatus name: 'Tests', state: 'running'
            bat "mvn test -X"
         }
         post {
                success {
                  updateGitlabCommitStatus name: 'Tests', state: 'success'
                }
                failure {
                  updateGitlabCommitStatus name: 'Tests', state: 'failed'
                }
            }
            
        }

        stage("Sonar") {
          steps {
            updateGitlabCommitStatus name: 'Sonar', state: 'running'
            bat "mvn clean verify sonar:sonar -Dsonar.projectKey=Timesheet -Dsonar.host.url=http://localhost:9000 -Dsonar.login=801ae6a994d3710c4e4974838ccb244c3c542f00"
         }
         post {
                success {
                  updateGitlabCommitStatus name: 'Sonar', state: 'success'
                }
                failure {
                  updateGitlabCommitStatus name: 'Sonar', state: 'failed'
                }
            }
            
        }
        
        stage("Deploy") {
          steps {
            updateGitlabCommitStatus name: 'Deploy', state: 'running'
            bat "mvn clean package  deploy:deploy-file  -DgroupId=tn.esprit.spring -DartifactId=timesheet_devops -Dversion=1.2-SNAPSHOT -DgeneratePom=true -Dpackaging=jar -DrepositoryId=deploymentRepo -Durl=http://localhost:8081/repository/maven-snapshots/ -Dfile=target/timesheet_devops-1.2-SNAPSHOT.jar"
          }
          post {
                success {
                  updateGitlabCommitStatus name: 'Deploy', state: 'success'
                }
                failure {
                  updateGitlabCommitStatus name: 'Deploy', state: 'failed'
                }
            }
        }

        stage('Docker Image') {
         steps {
           updateGitlabCommitStatus name: 'Docker Image', state: 'running'
          script {
          dockerImage = docker.build registry 
         }
        }
        post {
                success {
                  updateGitlabCommitStatus name: 'Docker Image', state: 'success'
                }
                failure {
                  updateGitlabCommitStatus name: 'Docker Image', state: 'failed'
                }
            }
      }

      stage('To Docker Hub') {
      steps {
        updateGitlabCommitStatus name: 'To Docker Hub', state: 'running'
         script {
            docker.withRegistry( '', registryCredential ) {
            dockerImage.push()
         }
        }
      }
      post {
                success {
                  updateGitlabCommitStatus name: 'To Docker Hub', state: 'success'
                }
                failure {
                  updateGitlabCommitStatus name: 'To Docker Hub', state: 'failed'
                }
            }
      }

     stage('Docker Compose') {
      steps {
        updateGitlabCommitStatus name: 'Docker Compose', state: 'running'
            bat "docker-compose up -d"
      }
              post {
                success {
                  updateGitlabCommitStatus name: 'Docker Compose', state: 'success'
                }
                failure {
                  updateGitlabCommitStatus name: 'Docker Compose', state: 'failed'
                }
            }
    }
  }
  post {
    always {
      emailext to: "oussama.amri1@esprit.tn",
      subject: "jenkins build:${currentBuild.currentResult}: ${env.JOB_NAME}",
      body: "${currentBuild.currentResult}: Job ${env.JOB_NAME}\nMore Info can be found here: ${env.BUILD_URL}"
    }
  }  
}
