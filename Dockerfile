FROM openjdk:8-jdk-alpine
EXPOSE 8083
COPY target/timesheet_devops-1.2-SNAPSHOT.jar timesheet_devops-1.2-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/timesheet_devops-1.2-SNAPSHOT.jar"]
