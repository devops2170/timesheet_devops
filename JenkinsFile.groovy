def docker_username = "mzorai";

def docker_password = "P@ssword123";


pipeline {
    agent any
    environment {
        registry = "mzorai/devopsimage"
        registryCredential= 'dockerHub'
        dockerImage = ''
    }
    stages {
        stage('clone and clean repo'){
            steps{
                cleanWs()
                bat "git clone https://gitlab.com/devops2170/timesheet_devops.git"

                bat "mvn clean -f timesheet_devops"

            }
        }

        stage('Package'){
            steps {
                bat "mvn package -DskipTests -f timesheet_devops"
            }
        }
        stage('Installation'){
            steps {
                bat "mvn install -DskipTests -f timesheet_devops"
            }
        }

        stage('Deploiement du livrable dans Nexus'){
            steps{

                bat "mvn deploy -DskipTests -f timesheet_devops"

            }
        }
        stage('SonarQube Test'){
            steps{
                bat "mvn sonar:sonar -f timesheet_devops"
            }
        }

        stage('Create Docker Image'){
            steps{
                bat "docker build -t ${docker_username}/devopsimage timesheet_devops"
                bat "docker login -u ${docker_username} -p ${docker_password}"
                bat "docker push ${docker_username}/devopsimage"
            }
        }


        stage('Starting Project') {
            steps { bat "copy timesheet_devops/docker-compose.yml ."
                bat "docker-compose up --build &"
            }
        }

    }
}
